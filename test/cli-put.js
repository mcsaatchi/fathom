'use strict';

var exec = require('child_process').exec;
var data = require('./support').data;

var dataupdate = JSON.stringify({
  results: [{
    message: 'updated message'
  }]
});

var invaliddataupdate = JSON.stringify({
  results: [{
    message: 'updated message'
  }, {
    message: 'updated message'
  }]
});


describe('bin/fathom -m PUT', function () {


  it('should exit with an error when PUT on classes without -d', function (done) {

    var cmd = 'node bin/fathom -m PUT -p classes/MyClass/xxx';

    exec(cmd, function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^ENODATA/);
      done();
    });
  });


  it('should exit with an error when PUT with invalid data', function (done) {


    var cmd = 'node bin/fathom -m PUT -p classes/MyClass/xxx -d \'' +
      invaliddataupdate + '\'';

    exec(cmd, function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^EINVDATA/);
      done();
    });
  });


  it('should exit with an error when PUT without objectId', function (done) {


    var cmd = 'node bin/fathom -m PUT -p classes/MyClass -d \'' +
      dataupdate + '\'';


    console.log(cmd);
    exec(cmd, function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^EMISNGID/);
      done();
    });
  });


  it('should exit with 0 when PUT single object on classes', function (done) {


    var updated, objectId,
      update= 'updated message',
      cmd = 'node bin/fathom -m POST -p classes/MyClass -d ' +
        data.one.source;


    exec(cmd, function (err, stdout) {
      objectId = JSON.parse(stdout).objectId;

      cmd = 'node bin/fathom -m PUT -p classes/MyClass/' + objectId +
        ' -d \'' + dataupdate + '\'';


      exec(cmd, function (err, stdout) {
        updated = JSON.parse(stdout);
        updated.should.have.key('updatedAt');


        cmd = 'node bin/fathom -m GET -p classes/MyClass/' + objectId;


        exec(cmd, function (err, stdout) {
          updated = JSON.parse(stdout);
          updated.objectId.should.eql(objectId);
          updated.message.should.eql(update);
          done();
        });
      });
    });


  });
});
