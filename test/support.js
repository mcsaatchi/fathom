'use strict';

var fs = require('fs');
var path = require('path');
var fathom = require('../lib/fathom');
var config = require('../lib/config');
var _ = require('lodash');


//----------------------------------
//
// Setup
//
//----------------------------------


var support = {};


var cwd = process.cwd();
var fixturespath = path.join(cwd, 'test/fixtures');
// This is the path where .fathomrc is actually supposed to be
var fathomrcpath = path.join(cwd, '.fathomrc');
var keys;


try {
  keys = JSON.parse(fs.readFileSync('./test/fixtures/keys.json'));
} catch (e) {
  var message = 'Before running the tests you need to create a ./test/fixures/keys.json file, see ./test/fixtures/_keys.json for reference.';
  message += '\n' + e.message;
  throw new Error(message);
}


//----------------------------------
//
// Commonly used fixture data
//
//----------------------------------


support.data = [
  {
    source: 'test/fixtures/invalid-data.json',
    name: 'invalid'
  },
  {
    source: 'test/fixtures/valid-data-one.json',
    name: 'one'
  },
  {
    source: 'test/fixtures/valid-data-many.json',
    name: 'many'
  },
].reduce(function (data, item) {
  data[item.name] = {
    raw: fs.readFileSync(item.source, 'utf-8'),
    source: item.source
  };
  return data;
}, {});


var copycfg = support.copycfg = function (src, done) {
  var cfg = fs.readFileSync(path.join(fixturespath, src), 'utf-8');
  cfg = _.template(cfg)(keys);
  fs.writeFileSync(fathomrcpath, cfg);
  done();
};


support.delcfg = function (done) {
  if (fs.existsSync(fathomrcpath)) {
    fs.unlink(fathomrcpath, done);
  } else {
    done();
  }
};


support.config = function () {
  return config();
};


support.fathom = function () {
  return fathom(config());
};


support.context = function (rcsrc, requestPath, data) {
  var context = {};

  var _init = function () {
    context.fathom = fathom(config());
    // The testing context is created with values taken
    // directly from the rc file, to make sure whatever
    // fathom is doing with the config will be the same
    // as using the rc file directly and re-creating
    // the values.
    var rc = JSON.parse(fs.readFileSync(fathomrcpath, 'utf-8'));
    context.requestPath = requestPath;
    context.data = data;
    context.path = '/' + rc.apiversion + '/' + requestPath;
    context.url = rc.apiurl.replace(/\/+$/, '') + context.path;
  };

  context.init = function (done) {
    copycfg(rcsrc, function () {
      _init();
      done();
    });
  };

  return context;
};


module.exports = support;
