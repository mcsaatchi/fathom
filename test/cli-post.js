'use strict';


var exec = require('child_process').exec;
var data = require('./support').data;


/* jshint -W030 */
describe('bin/fathom -m POST', function () {


  it('should exit with 0 when POST with valid data', function (done) {

    var cmd = 'node bin/fathom -m POST -p classes/MyClass -d ' +
      data.one.source;

    exec(cmd, function (err, stdout) {
      var response = JSON.parse(stdout);
      response.createdAt.should.not.be.empty;
      response.objectId.should.not.be.empty;
      done();
    });
  });


  it('should exit with 0 when POST with single valid inlined data', function (done) {

    var cmd = 'node bin/fathom -m POST -p classes/MyClass -d \'' +
      data.one.raw + '\'';

    exec(cmd, function (err, stdout) {
      var response = JSON.parse(stdout);
      response.createdAt.should.not.be.empty;
      response.objectId.should.not.be.empty;
      done();
    });
  });


  it('should exit with 0 when POST with multiple valid inlined data', function (done) {

    var cmd = 'node bin/fathom -m POST -p classes/MyClass -d \'' +
      data.many.raw + '\'';

    exec(cmd, function (err, stdout) {
      var response = JSON.parse(stdout);
      response.forEach(function(item) {
        item.success.should.not.be.empty;
        item.success.createdAt.should.not.be.empty;
        item.success.objectId.should.not.be.empty;
      });
      done();
    });
  });


  it('should exit with 0 when POST with valid multiple data', function (done) {

    var cmd = 'node bin/fathom -m POST -p classes/MyClass -d ' +
      data.many.source;

    exec(cmd, function (err, stdout) {
      var response = JSON.parse(stdout);
      response.forEach(function(item) {
        item.success.should.not.be.empty;
        item.success.createdAt.should.not.be.empty;
        item.success.objectId.should.not.be.empty;
      });
      done();
    });
  });


});
/* jshint +W030 */
