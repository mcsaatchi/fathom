'use strict';


var assert = require('assert');
var exec = require('child_process').exec;


/* jshint -W030 */
describe('bin/fathom misc', function () {


  it('should exit with 0 when passed -h', function (done) {

    exec('node bin/fathom -h', function (err) {
      // https://github.com/visionmedia/should.js/issues/27
      assert(!err);
      done();
    });
  });


});
/* jshint +W030 */
