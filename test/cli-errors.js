'use strict';


var exec = require('child_process').exec;
var data = require('./support').data;


describe('bin/fathom errors', function () {


  it('should exit with 1 when GET on non existing object', function (done) {

    var cmd = 'node bin/fathom -m GET classes/MyClass/nothinghere';

    exec(cmd, function (err) {
      err.code.should.eql(1); done();
    });
  });


  it('should exit with 1 when DELETE on non existing object', function (done) {

    var cmd = 'node bin/fathom -m DELETE classes/MyClass/nothinghere';

    exec(cmd, function (err) {
      err.code.should.eql(1);
      done();
    });
  });


  it('should exit with 1 when arguments not given', function (done) {

    exec('node bin/fathom', function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^EPATH/);
      done();
    });
  });


  it('should exit with 1 when POST with no request path', function (done) {

    exec('node bin/fathom -m POST', function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^EPATH/);
      done();
    });
  });


  it('should exit with 1 when POST with no data', function (done) {

    exec('node bin/fathom -m POST -p classes/MyClass', function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^ENODATA/);
      done();
    });
  });


  it('should exit with 1 when the data file is not found', function (done) {

    var cmd = 'node bin/fathom -m POST -p classes/MyClass -d foo/bar.json';

    exec(cmd, function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^EMISNGD/);
      done();
    });
  });


  it('should exit with 1 when the inlined data file is not valid', function (done) {

    var cmd = 'node bin/fathom -m POST -p classes/MyClass -d \'' +
      data.invalid.raw + '\'';

    exec(cmd, function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^EMISNGD/);
      done();
    });
  });


  it('should exit with 1 when GET multiple objects', function (done) {
    var cmd = 'bin/fathom -m GET -p classes/MyClass -d \'' +
      data.many.raw + '\'';

    exec(cmd, function (err, stdout, stderr) {
      err.code.should.eql(1);
      stderr.should.match(/^ENOSUPP/);
      done();
    });
  });


});
