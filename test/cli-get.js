'use strict';


var exec = require('child_process').exec;
var data = require('./support').data;


/* jshint -W030 */
describe('bin/fathom -m GET', function () {
  it('should exit with 0 when GET single object', function (done) {

    var postcmd = 'bin/fathom -m POST -p classes/MyClass -d ' +
      data.one.source;
    var getcmd = 'bin/fathom -m GET -p classes/MyClass/';

    exec(postcmd, function (err, stdout) {
      var objectId = JSON.parse(stdout).objectId;
      getcmd += objectId;

      exec(getcmd, function (err, stdout) {
        var response = JSON.parse(stdout);
        response.objectId.should.eql(objectId);
        done();
      });
    });
  });
});
/* jshint +W030 */
