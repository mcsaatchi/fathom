'use strict';


var exec = require('child_process').exec;
var data = require('./support').data;


/* jshint -W030 */
describe('bin/fathom -m DELETE', function () {


  it('should exit with 0 when DELETE single object', function (done) {

    var postcmd = 'node bin/fathom -m POST -p classes/MyClass -d ' +
      data.one.source;
    var delcmd = 'node bin/fathom -m DELETE -p classes/MyClass/';

    exec(postcmd, function (err, stdout) {
      var objectId = JSON.parse(stdout).objectId;

      exec(delcmd + objectId, function (err, stdout) {
        var response = JSON.parse(stdout);
        response.should.be.empty;
        done();
      });
    });
  });


  it('should exit with 0 when DELETE multiple objects', function (done) {

    var postcmd = 'node bin/fathom -m POST -p classes/MyClass -d ' +
      data.many.source;
    var delcmd = 'node bin/fathom -m DELETE -p classes/MyClass -d \'';

    exec(postcmd, function (err, stdout) {
      var response = JSON.parse(stdout);
      var d = { results: [] };
      d.results = response.map(function (object) {
        return { objectId: object.success.objectId };
      });
      delcmd += JSON.stringify(d) + '\'';

      exec(delcmd, function (err, stdout) {
        var response = JSON.parse(stdout);
        response.forEach(function(item) {
          item.success.should.be.ok;
        });
        stdout.should.not.be.empty;
        done();
      });
    });
  });


  it('should exit with 0 when DELETE all objects', function (done) {
    var cmd = 'node bin/fathom -m DELETE -p classes/MyClass';

    exec(cmd, function (err, stdout) {
      var response = JSON.parse(stdout);
      response.should.not.be.emtpy;
      done();
    });
  });


});
/* jshint +W030 */
