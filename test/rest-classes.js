'use strict';

var path = require('path');
var support = require('./support');

describe('fathom.rest(): classes', function () {
  var requestPath = 'classes/MyClass';
  var data = { message: 'fathom.rest() with /1/classes' };
  var context = support.context('valid.fathomrc', requestPath, data);

  // Prevents occasional initial dirty state
  before(function (done) {
    support.delcfg(function() {
      context.init(done);
    });
  });

  describe('rest("POST", "nullpath")', function () {
    it('status code should equal 404', function (done) {
      context.fathom.rest('POST', 'nullpath', null, function (err, response) {
        response.statusCode.should.eql(404);
        done();
      });
    });
  });

  describe('rest("POST", "classes/MyClass", { data })', function () {
    it('status code should equal 201', function (done) {
      context.fathom.rest('POST', requestPath, data, function (err, response) {
        response.statusCode.should.eql(201);
        done();
      });
    });
  });

  describe('rest("GET", "classes/MyClass")', function () {
    it('should match local object definition', function (done) {
      context.fathom.rest('POST', requestPath, data, function (err, response, body) {
        context.fathom.rest('GET', path.join(requestPath, body.objectId), null, function(err, response, body) {
          var p;
          for (p in data) {
            body[p].should.eql(data[p]);
          }
          done();
        });
      });
    });
  });

  describe('rest("DELETE", "classes/MyClass/objectId")', function () {
    it('status code should equal 200', function (done) {
      context.fathom.rest('POST', requestPath, data, function (err, response, body) {
        context.fathom.rest('DELETE', path.join(requestPath, body.objectId), null, function(err, response) {
          response.statusCode.should.eql(200);
          done();
        });
      });
    });
  });

});
