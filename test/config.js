'use strict';

var support = require('./support');

describe('config()', function () {

  // Prevents occasional initial dirty state
  before(function (done) {
    support.delcfg(done);
  });

  afterEach(function (done) {
    support.delcfg(done);
  });

  describe('missing .fathomrc', function () {
    it('should throw ENOCFG when .fathomrc is not found', function () {
      support.config.should.throw(/^ENOCFG/);
    });
  });

  describe('invalid .fathomrc', function () {
    before(function (done) {
      support.copycfg('invalid.fathomrc', done);
    });
    it ('should throw EINVCFG when .fathomrc is not valid', function () {
      support.config.should.throw(/^EINVCFG/);
    });
  });

  describe('empty .fathomrc', function () {
    before(function (done) {
      support.copycfg('empty.fathomrc', done);
    });
    it('should be considered valid', function () {
      support.config();
    });
  });

  describe('valid .fathomrc', function () {
    before(function (done) {
      support.copycfg('valid.fathomrc', done);
    });
    it('should be considered valid', function () {
      support.config();
    });
  });

});
