'use strict';

var support = require('./support');

describe('fathom()', function () {
  var requestPath = 'classes/MyClass';

  // Prevents occasional initial dirty state
  before(function (done) {
    support.delcfg(done);
  });

  afterEach(function(done) {
    support.delcfg(done);
  });

  describe('empty .fathomrc', function () {
    before(function (done) {
      support.copycfg('empty.fathomrc', done);
    });
    it('should throw EMISNGP when missing required properties', function () {
      support.fathom.should.throw(/^EMISNGP/);
    });
  });

  describe('valid .fathomrc', function () {
    before(function(done) {
      support.copycfg('valid.fathomrc', done);
    });
    it('should create a fathom object', function () {
      support.fathom();
    });
  });

  describe('fathom.url()', function () {
    var context = support.context('valid.fathomrc', requestPath);
    before(context.init);

    it('should match fully qualified fathom.url', function () {
      context.fathom.url(requestPath).should.eql(context.url);
    });

    it('should match absolute path fathom.url', function () {
      context.fathom.url(requestPath, true).should.eql(context.path);
    });
  });

  describe('fathom.url() with trailing slashes in rc', function () {
    var context = support.context('valid-trailing-slash.fathomrc', requestPath);
    before(context.init);

    it('should match fully qualified fathom.url ignoring trailing slashes', function () {
      context.fathom.url(requestPath).should.eql(context.url);
    });
  });
});
