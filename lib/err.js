var strings = require('./strings');

module.exports = function (name, options) {
  options = options || {};
  var e = options.e;
  var etc = options.etc;
  var message = name + ' ' + strings[name];
  if (etc) {
    message += ' ' + etc;
  }
  if (e) {
    message += ': ' + e.message;
  }
  var _e = new Error(message);
  return _e;
};
