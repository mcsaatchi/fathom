'use strict';

var path = require('path');
var request = require('request');
var err = require('./err');
var support = require('./support');

var __config = {};


//--------------------------------------------------------------------------
//
// Utils and support
//
//--------------------------------------------------------------------------


var known = function (list) {
  return function(token) {
    return list.indexOf(token) > -1;
  };
};


var validData = function (rawData, onlyOne) {


    if (!rawData) {
      throw err('ENODATA');
    }

    // not an object
    var a = !support.isObject(rawData);

    // missing results property
    var b = !rawData.results;

    // results is not an array
    var c = !Array.isArray(rawData.results);

    // results array is empty (lazy)
    var d = function () {
      return !rawData.results.length;
    };

    // only one? (lazy)
    // onlyOne makes sense when an operation takes data as a parameter
    // but just needs one object, not an array of objects.
    var e = function () {
      if (onlyOne) {
        return rawData.results.length !== 1;
      }
    };

    if ( a || b || c || d() || e()) {
      throw err('EINVDATA');
    }


    return rawData.results;
};


//--------------------------------------------------------------------------
//
// Operation implementations
//
//--------------------------------------------------------------------------


//----------------------------------
//
// GET
//
//----------------------------------


var getMany = function () {
  throw err('ENOSUPP', { etc: ': Method \'GET\' is not supported in batch operations.' });
};

var getOne = function (reqPath, objectId) {
  var options = support.request(__config, {
    method: 'GET',
    reqPath: path.join(reqPath, objectId)
  });

  return function(callback) {
    return request(options, callback);
  };
};


var getAll = function (reqPath) {
  var options = support.request(__config, {
    method: 'GET',
    reqPath: reqPath
  });

  return function (callback) {
    return request(options, callback);
  };
};


//----------------------------------
//
// DELETE
//
//----------------------------------


var deleteOne = function (reqPath, objectId) {
  var options  = support.request(__config, {
    method: 'DELETE',
    reqPath: path.join(reqPath, objectId)
  });

  return function (callback) {
    return request(options, callback);
  };
};


var deleteMany = function (reqPath, objectId, data) {

  var requests = data.results.map(function (object) {
    return {
      path: support.url(__config, path.join(reqPath, object.objectId), true),
      method: 'DELETE'
    };
  });

  var options = support.request(__config, {
    method: 'POST',
    reqPath: 'batch',
    data: { requests: requests }
  });

  return function (callback) {
    return request(options, callback);
  };
};


var deleteAll = function (reqPath) {
  var collectObjects = getAll(reqPath);

  return function (callback) {

    collectObjects(function (err, result) {

      if (err || result.statusCode >= 400) {
        return callback(err || result.body);
      }

      var deleteObjects = deleteMany(reqPath, null, result.body);

      return deleteObjects(callback);

    });
  };
};


//----------------------------------
//
// POST
//
//----------------------------------


var postOne = function (reqPath, object) {

  var options = support.request(__config, {
    method: 'POST',
    reqPath: reqPath,
    data: object
  });

  return function (callback) {
    return request(options, callback);
  };
};


var postMany = function (reqPath, objects) {

  var requests = objects.map(function (object) {
    return {
      path: support.url(__config, reqPath, true),
      method: 'POST',
      body: object
    };
  });

  var options = support.request(__config, {
    method: 'POST',
    reqPath: 'batch',
    data: { requests: requests }
  });

  return function (callback) {
    return request(options, callback);
  };
};


//----------------------------------
//
// PUT
//
//----------------------------------


var putOne = function (reqPath, objectId, object) {

  var options = support.request(__config, {
    method: 'PUT',
    reqPath: path.join(reqPath, objectId),
    data: object
  });

  return function (callback) {
    return request(options, callback);
  };
};


//--------------------------------------------------------------------------
//
// operation support
//
//--------------------------------------------------------------------------


//----------------------------------
//
// simpleResolver
//
//----------------------------------


var simpleResolver = function (method, one, many, all) {
  return function (reqPath, objectId, data) {

    var isString = support.isString(objectId);
    var isObject = support.isObject(data);
    var oneOrMany = objectId || data;

    // The premise is: in the presence of both objectId and data,
    // at least one of them needs to be valid in order to proceed,
    // otherwise user can unintentionally perform operations on "all",
    // like deleteAll() for instance.
    // Being both objectId and data present, objectId ("one") will take
    // precedence.

    if (oneOrMany && !isString && !isObject) {
      throw err('EINVPRAM', { etc: '\n' + [
        'method: ' + method,
        'objectId: ' + objectId,
        'data: ' + data
      ].join('\n')});
    }

    if (isString) {
      return one(reqPath, objectId);
    }

    if (isObject) {
      return many(reqPath, objectId, data);
    }

    return all(reqPath);

  };
};


//--------------------------------------------------------------------------
//
// operations
//
//--------------------------------------------------------------------------


var operations = {


  'GET': simpleResolver('GET', getOne, getMany, getAll),


  'DELETE': simpleResolver('DELETE', deleteOne, deleteMany, deleteAll),


  'POST': function (reqPath, objectId, data) {

    var objects = validData(data);

    if (objects.length === 1) {
      return postOne(reqPath, objects[0]);
    }

    return postMany(reqPath, objects);
  },


  'PUT': function (reqPath, objectId, data) {

    if (!objectId) {
      throw err('EMISNGID');
    }

    var objects = validData(data, true);

    return putOne(reqPath, objectId, objects[0]);
  }
};

var knownMethod = known(Object.keys(operations));
var knownPath = known(['classes']);

/*
 * Operation: one or more sendable operations implemented by Fathom
 * on the Parse API.
 *
 *  reqPath
 *    GET
 *      objectId                          getOne(reqPath, objectId)
 *      data                              getMany(reqPath, data)
 *      null                              getAll(reqPath)
 *
 *    DELETE
 *      objectId                          deleteOne(reqPath, objectId)
 *      data                              deleteMany(reqPath, data)
 *      null                              deleteAll(reqPath)
 *
 *    POST
 *      data.results === 1                postOne(reqPath, data.results[0])
 *      data.results > 1                  postMany(reqPath, data.results)
 *
 *    PUT
 *      objectId && data.results === 1    putOne(reqPath, objectId, data)
 *
 */
module.exports = function (config, method, reqPath, data) {

  var error = err('EPATH', { etc: ': path: ' + reqPath });

  if (!reqPath || !support.isString(reqPath)) {
    throw error;
  }

  var pathSplit = reqPath.split('/');
  var invalidPath = pathSplit.length < 2;
  var unknownPath = !knownPath(pathSplit[0]);

  if (invalidPath || unknownPath ) {
    throw error;
  }

  if (!knownMethod(method)) {
    throw err('EMETHOD', { etc: ': method: ' + method });
  }

  __config = config;

  reqPath = path.join(pathSplit[0], pathSplit[1]);
  var objectId = pathSplit[2];

  // console.log(method);
  // console.log(reqPath);
  // console.log(objectId);
  // console.log(data);

  return operations[method](reqPath, objectId, data);
};
