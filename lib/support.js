'use strict';

var fs = require('fs');
var path = require('path');
var url = require('url');
var err = require('./err');

var required = [
  'apiurl',
  'apiversion',
  'appid',
  'restapi',
];

var support = {};

support.parseData = function (source) {
  var data, etc;

  try {

    // try to parse the source input directly first
    data = JSON.parse(source);

  } catch (e) {
    etc = '\n' + err('EINVDATA').message + '\n' + e.message;

    try {
      // then assume source is a path and try to load a file
      data = fs.readFileSync(source, 'utf-8');
      data = JSON.parse(data);

    } catch (e) {
      etc += '\n' + e.message;
      throw err('EMISNGD', { etc: etc });
    }
  }

  return data;
};

support.url = function (config, forPath, makeRelative) {
  var _path = path.join('/', config.apiversion.toString(), forPath);

  if (makeRelative) {
    return _path;
  }

  return url.resolve(config.apiurl, _path);
};


support.request = function (config, description) {
  return {
    method: description.method,
    url: support.url(config, description.reqPath),
    json: true,
    headers: {
      'X-Parse-Application-Id': config.appid,
      'X-Parse-REST-API-Key': config.restapi,
    },
    body: description.data ? JSON.stringify(description.data) : undefined,
  };
};


support.validateConfig = function (config) {

  var missing = required.reduce(function (memo, property) {
    if (!config[property]) {
      memo.push(property);
    }
    return memo;
  }, []);

  if (missing.length) {
    throw err('EMISNGP', { etc: missing.join(', ') });
  }

  return config;
};

//----------------------------------
//
// Borrowed from _
//
//----------------------------------

support.isObject = function (obj) {
  return obj === Object(obj);
};

[
  'Arguments',
  'Function',
  'String',
  'Number',
  'Date',
  'RegExp'
].forEach(function (name) {
  support['is' + name] = function (obj) {
    return Object.prototype.toString.call(obj) === '[object ' + name + ']';
  };
});

module.exports = support;
