'use strict';

var request = require('request');
var support = require('./support');
var operation = require('./operation');

var fathom = {

  url: function (forPath, makeRelative) {
    return support.url(this.config, forPath, makeRelative);
  },

  /*
   * Direct operation on the Parse API.
   */
  rest: function (method, reqPath, data, callback) {
    var options = support.request(this.config, {
      method: method || 'GET',
      reqPath: reqPath,
      data: data
    });
    return request(options, callback);
  },

  /*
   * Resolves to a known operation on the Parse API (as implemented by Fathom),
   * implementation by inspecting the given arguments.
   * Operations can be composed from more than one `fathom.rest()` call.
   */
  send: function (method, reqPath, data, callback) {
    var send = operation(this.config, method, reqPath, data);
    return send(callback);
  }
};

module.exports = function (config) {
  var descriptors = {
    'config': {
      value: support.validateConfig(config),
      enumerable: true,
      configurable: false,
      writable: false
    }
  };
  return Object.create(fathom, descriptors);
};
