'use strict';

var path = require('path');
var fs = require('fs');
var err = require('./err');

var fathomrcpath = path.resolve(process.cwd(), '.fathomrc');

function loadConfigFile () {
  var cfg;

  try {
   cfg = fs.readFileSync(fathomrcpath, 'utf-8');
  } catch (e) {
    throw err('ENOCFG', { e: e });
  }

  try {
    cfg = JSON.parse(cfg);
  } catch (e) {
    throw err('EINVCFG', { e: e });
  }

  return cfg;
}

module.exports = function () {
  var source = loadConfigFile();
  var descriptors = {}, p;
  for (p in source) {
    descriptors[p] = {
      enumerable: true,
      configurable: false,
      writable: false,
      value: source[p]
    };
  }
  var config = Object.create(null, descriptors);
  return Object.freeze(config);
};
