'use strict';

module.exports = function (grunt) {
  grunt.initConfig({
    jshint: {
      gruntfile: {
        src: 'gruntfile.js',
        options: {
          jshintrc: '.jshintrc'
        }
      },
      bin: {
        src: ['bin/fathom'],
        jshintrc: 'bin/.jshintrc'
      },
      lib: {
        src: ['lib/**/*.js'],
        options: {
          jshintrc: 'lib/.jshintrc'
        }
      },
      test: {
        src: ['test/**/*.js'],
        options: {
          jshintrc: 'test/.jshintrc'
        }
      }
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      bin: {
        files: '<%= jshint.bin.src %>',
        tasks: ['jshint:bin', 'mocha']
      },
      lib: {
        files: '<%= jshint.lib.src %>',
        tasks: ['jshint:lib', 'mocha']
      },
      test: {
        files: '<%= jshint.test.src %>',
        tasks: ['jshint:test', 'mocha']
      },
      fixtures: {
        files: 'test/fixtures/**/*',
        tasks: ['mocha']
      }
    },
    shell: {
      mocha: {
        options: {
          stdout: true,
          stderr: true,
        },
        command: 'mocha'
      }
    }
  });

  grunt.registerTask(
    'mocha',
    'A proxy for shell:mocha, decorates shell:moch with argumens from the command line',
    function () {
      var grep = grunt.option('grep');
      if (grep) {
        grunt.config('shell.mocha.command', 'mocha --grep "' + grep + '"');
      }
      grunt.task.run(['shell:mocha']);
  });

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  grunt.registerTask('default', [
    'jshint',
    'mocha'
  ]);

};
