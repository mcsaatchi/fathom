# Fathom

Fathom is a Node.js command line application that communicates with [Parse](http://parse.com) using the [REST API](https://parse.com/docs/rest).

## Installation

```
~ npm install -g fathom
```

## Configuration

Create a `.fathomrc` file at the root directory of your project:

```
{
    "restapi": "[ REST API key ]",
    "appid": "[ Application ID ]",
    "apiurl": "https://api.parse.com",
    "apiversion": 1
}
```

## Usage

```
~ fathom -h

    Usage: fathom [options]

    Options:

        -h, --help                      output usage information
        -V, --version                   output the version number
        -p, --path [String]             Required: request path
        -m, --method [GET|POST|DELETE]  Optional: request method, defaults to "GET"
        -d, --data [String]             Optional: request payload, JSON String or path to JSON file.
```

## Creating Objects

Fathom uses Parse import data format, read more about it [here](https://parse.com/docs/data#data-import).

### Using inlined JSON data:

```
fathom -m POST -p classes/MyClass -d '{"results": [{"name":"My First Object"}]}'
```

### Using a JSON file:

data.json:

```
{
    "results":
    [
        {
            "name": "My First Object"
        }
    ]
}
```

Then:

```
~ fathom -m POST -p classes/MyClass -d data.json

{
    "createdAt": "2014-02-16T12:59:25.869Z",
    "objectId": "ik9DjkW8kH"
}
```

### Creating many objects at once

Internally implemented using using a [batch operation](https://parse.com/docs/rest#objects-batch)

data.json

```
{
    "results":
    [
        {
            "name": "My First Object"
        },
        {
            "name": "My Second Object"
        },
        {
            "name": "My Third Object"
        }
    ]
}
```

Then:

```
~ fathom -m POST -p classes/MyClass -d data.json

[
    {
        "success": {
            "createdAt": "2014-02-16T13:08:44.275Z",
            "objectId": "u9Y4JBVaks"
        }
    },
    {
        "success": {
            "createdAt": "2014-02-16T13:08:44.303Z",
            "objectId": "5jTwudV7zk"
        }
    },
    {
        "success": {
            "createdAt": "2014-02-16T13:08:44.319Z",
            "objectId": "qC60wI5Fel"
        }
    }
]
```


## Retrieving Objects

### Retrieving one object

```
~ fathom -m GET -p classes/MyClass/u9Y4JBVaks

{
    "createdAt": "2014-02-16T13:08:44.275Z",
    "name": "My First Object",
    "objectId": "u9Y4JBVaks",
    "updatedAt": "2014-02-16T13:08:44.275Z"
}
```

### Retrieving all objects from a class

```
~ fathom -m GET -p classes/MyClass

{
    "results": [
        {
            "name": "My First Object",
            "createdAt": "2014-02-16T13:08:44.275Z",
            "updatedAt": "2014-02-16T13:08:44.275Z",
            "objectId": "u9Y4JBVaks"
        },
        {
            "name": "My Second Object",
            "createdAt": "2014-02-16T13:08:44.303Z",
            "updatedAt": "2014-02-16T13:08:44.303Z",
            "objectId": "5jTwudV7zk"
        },
        {
            "name": "My Third Object",
            "createdAt": "2014-02-16T13:08:44.319Z",
            "updatedAt": "2014-02-16T13:08:44.319Z",
            "objectId": "qC60wI5Fel"
        }
    ]
}
```

## Deleting Objects

### Deleting one object

```
~ fathom -m DELETE -p classes/MyClass/u9Y4JBVaks

{}
```

### Deleting many objects

Internally implemented using using a [batch operation](https://parse.com/docs/rest#objects-batch)

data.json

```
{
    "results": [
        {
            "objectId": "5jTwudV7zk"
        },
        {
            "objectId": "qC60wI5Fel"
        }
    ]
}
```

Then:

```
~ fathom -m DELETE -p classes/MyClass -d data.json

[
    {
        "success": true
    },
    {
        "success": true
    }
]
```

### Deleting all objects from a class

Internally implemented using using a [batch operation](https://parse.com/docs/rest#objects-batch)


```
~ fathom -m DELETE -p classes/MyClass

[
    {
        "success": true
    },
    {
        "success": true
    },
    {
        "success": true
    }
]
```


## Updating objects

Given you already know ```objectId```, you can create a data.json file:

```
{
    "results":
    [
        {
            "message": "hello"
        }
    ]
}
```

Then:

```
~ fathom -m PUT -p classes/MyClass/ik9DjkW8kH -d data.json

{
    "updatedAt": "2014-03-01T14:22:45.103Z"
}
```

## Using inlined JSON

All the operations that take a JSON file as argument can take inline JSON as well:

```
~ fathom -m PUT -p classes/MyClass/8eccnwrv7L -d '{"results": [{"message":"hello"}]}'
```

## Roadmap

-   0.1 (Released):
    -   GET, POST, DELETE on ```classes```

-   0.2:
    -   PUT on classes

-   0.3:
    -   POST, PUT, GET, DELETE on ```users```
    -   query strings
    -   Session tokens
    -   GET on ```login```
    -   GET on ```users/me```

-   0.4:
    -   Master key on configuration
    -   POST, PUT, GET, DELETE on ```roles```

-   0.5 Nice to have:
    -   pipes
    -   reformat: batch to results
    -   reformat: results to batch

## Changelog

### v0.2.1
- date: 2014-03-12
- changes:
    -   update on readme.md
    -   update on cli help

### v0.2.0

- date: 2014-03-01
- changes:
    -   PUT on classes

### v0.1.2:

-   date: 2014-02-16
-   changes:
    -   readme.md

### v0.1.1:

-   date: 2014-02-16
-   changes:
    -   removing -o --objectid from CLI API
    -   changing data input format to match parse [import](https://parse.com/docs/data#data-import) format
    -   allowing inline JSON input for -d --data

### v0.1.0:

-   date: 2014-02-12
-   changes:
    -   GET, POST, DELETE on ```classes```
    -   CLI API
    -   CLI help
